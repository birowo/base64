package b64

/* non RFC standard version of base64 algorithm */
/* simple algorithm for better performance than the standard version */

import (
	"errors"
)

const z = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-_"

func FromByteSlc(dest, src []byte) {
	bit67 := byte(0)
	j, k := 0, 2
	for i, chr := range src {
		dest[j] = z[chr&63]
		j++
		bit67 = (bit67 | (chr & 192)) >> 2
		if i == k {
			dest[j] = z[bit67]
			j++
			k += 3
		}
	}
	switch k - len(src) {
	case 1:
		dest[j] = '='
		dest[j+1] = '='
		dest[j+2] = z[bit67>>4]
	case 0:
		dest[j] = '='
		dest[j+1] = z[bit67>>2]
	}
}

var y = func() (y [256]byte) {
	for i := 0; i < 256; i++ {
		if i > 47 && i < 58 {
			y[i] = byte(i - 48)
		} else if i > 96 && i < 123 {
			y[i] = byte(i - 87)
		} else if i > 64 && i < 91 {
			y[i] = byte(i - 29)
		} else if i == 45 {
			y[45] = 62
		} else if i == 95 {
			y[95] = 63
		} else {
			y[i] = 64
		}
	}
	return
}()

func ToByteSlc(dest, src []byte) (err error) {
	last := len(src) - 1
	switch byte('=') {
	case src[last-2]:
		last -= 2
	case src[last-1]:
		last--
	}
	z := y[src[3]]
	if z == 64 {
		err = errors.New("invalid char: " + string(z))
		return
	}
	j, k := 0, 3
	for i, chr := range src[:last] {
		if i == k {
			k += 4
			z = y[src[k]]
			if z == 64 {
				err = errors.New("invalid char: " + string(z))
				return
			}
			continue
		}
		x := y[chr]
		if x == 64 {
			err = errors.New("invalid char: " + string(x))
			return
		}
		dest[j] = (z << 6) | x
		z >>= 2
		j++
	}
	return
}
