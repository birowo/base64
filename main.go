package main

import (
	"fmt"

	"gitlab.com/birowo/base64/lib"
	"lukechampine.com/frand"
)

func main() {
	a0 := make([]byte, 24)
	b0 := make([]byte, 32)
	c0 := make([]byte, 24)
	frand.Read(a0)
	b64.FromByteSlc(b0, a0)
	b64.ToByteSlc(c0, b0)
	fmt.Println(a0, "\n", string(b0), "\n", c0, "\n\n")

	a1 := make([]byte, 23)
	b1 := make([]byte, 32)
	c1 := make([]byte, 23)
	frand.Read(a1)
	b64.FromByteSlc(b1, a1)
	b64.ToByteSlc(c1, b1)
	fmt.Println(a1, "\n", string(b1), "\n", c1, "\n\n")

	a2 := make([]byte, 22)
	b2 := make([]byte, 32)
	c2 := make([]byte, 22)
	frand.Read(a2)
	b64.FromByteSlc(b2, a2)
	b64.ToByteSlc(c2, b2)
	fmt.Println(a2, "\n", string(b2), "\n", c2, "\n\n")
}
